/**************************************
 * EXPORT/INCLUDE REQUIRED LIBRARIES 
**************************************/
const AWS               =   require('aws-sdk');
const bcrypt            =   require('bcryptjs');
const {createPublicKey} =   require('crypto');
const fs                =   require('fs');
const jwt               =   require('jsonwebtoken');
const nf                =   require('node-forge').pki;
const rsa               =   require('node-rsa');

// CONFIGURE AWS REGION
AWS.config.update({region: process.env.AWS_REGION});

const ddb               =   new AWS.DynamoDB({apiVersion: '2012-08-10'});
const s3                =   new AWS.S3();
const saltRounds        =   10;
const bucketName        =   process.env.S3_bucket;
const ddbtable          =   process.env.Ddb_table_name;

// tested this in version node 17, this library is supported in node version > 16
// var {X509Certificate , createPublicKey} = require('crypto');
// var x509 = new X509Certificate(fs.readFileSync('dormakabatt.crt'));

/*******************************************************
    LAMBDA HANDLER 
********************************************************/
exports.handler = (event, context, callback) => { 

    var certFileName = JSON.parse(JSON.stringify(event)).certificate_name;

    readFile(bucketName, certFileName, readCertFileAndGenerateHash, onError);
};

/*************************************************************************************************************
    READS THE FILE FROM S3, IF THE FILE EXIST THEN CALL THE METHOD readCertFileAndGenerateHash()
*************************************************************************************************************/
function readFile (bucketName, fileName, readCertFileAndGenerateHash, onError) 
{
    var params = { Bucket: bucketName, Key: fileName };
    
    s3.getObject(params, function (err, data) 
    {
        if (!err) 
        {
            var action = readCertFileAndGenerateHash(data.Body.toString());
            if (action == true)
                console.log('Action successfully performed');
            else
                console.log('Failed to perform the action');
        }
        else
        {
            console.error(err);
            const message = `Error getting object ${fileName} from bucket ${bucketName}. Make sure they exist and your bucket is in the same region as this function.`;
            console.error(message);
        }
    });
    
}

/*************************************************************************************************************
    READS THE CERT/PEM FILE FROM S3
    EXTRACT COMMON NAME FROM THE SUBJECT OF THE CERT/PEM USING THE METHOD extractCommonName()
    EXTRACT THE PUBLIC KEY FROM THE CERT/PEM USING THE METHOD extractPublicKey()
    GENERATE THE NEW PRIVATE KEY USING RSA ALGORITHM USING THE METHOD generatePrivateKey()
    GENERATE JWT TOKEN USING THE METHOD generateJWTtoken()
    GENEATE THE PASSWORD HASH USING THE METHOD hashgenerator()
    ADD common_name AND passwordhash TO THE DYNAMO DB TABLE
*************************************************************************************************************/
async function readCertFileAndGenerateHash(content) 
{
    // console.debug('conetnet:'+content);
    var certContent = nf.certificateFromPem(content)
    var commonName = extractCommonName(certContent);
    console.debug('common name is: '+commonName);
    var publickey = extractPublicKey(content);
    var privatekey = generatePrivateKey();

    signInToken = generateJWTtoken(privatekey);
    // console.log(signInToken)
    var passHash = await hashgenerator(signInToken);
    console.log('hash value: '+passHash);

    var dbaction = addRowsToUserAuth(commonName, passHash)

    return dbaction;
}

/******************************************************
    SAVE ROWS IN THE DYNAMODB TABLE
******************************************************/
function addRowsToUserAuth(commonName, passHash)
{
    var params = 
    {
        TableName: ddbtable,
        Item: {
          'common_name' : {S: commonName},
          'password_hash' : {S: passHash}
        }
      };
    
    ddb.putItem(params, function(err, data) 
    {
        if (err)
        {
            console.log("Error", err);
            return false;
        }
        else
        {
            console.log("Success", data);
            return true;
        }
    });
}

/******************************************************
    GENERATES PASSWORD HASH USING bcrypt library
******************************************************/
async function hashgenerator(signInToken)
{
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(signInToken, salt);
    return hash;
}

/******************************************************
    ERROR MEHTOD CALLED ON LAMBDA FAILIURE
******************************************************/
function onError (err) 
{
    console.log('error: ' + err);
}  

/******************************************************
 *  METHOD TO EXTRACT COMMON NAME FROM THE CERT/PEM
******************************************************/
function extractCommonName(certContent)
{
    console.log('Started fetching the common name from certificate');
    var subject = certContent.subject.attributes.map(attr => [attr.shortName, attr.value].join()).join('::');
    // console.log('subject of the certificate :'+ subject);
    var pos = subject.indexOf('CN,')
    var pos1 = subject.indexOf('::' , pos)
    var cn = (pos1 > 0) ? subject.substr(pos+3, pos1-(pos+3)) : subject.substr(3);
    return cn
}

/******************************************************
 *  METHOD TO EXTRACT PUBLIC KEY FROM CERT/PEM
******************************************************/
function extractPublicKey(certContent)
{
    console.log('Started extracting public key from the certificate');
    // extract public key from the certificate
    // var publickey = createPublicKey(x509.toString()).export({type:'spki', format:'pem'})    
    var publickey = createPublicKey(certContent).export({type:'spki', format:'pem'});
    // console.debug('publickey: '+publickey);
    return publickey
}

/***********************************************************
 *  METHOD TO GENERATE PRIVATE KEY USING node-rsa library
************************************************************/
function generatePrivateKey()
{
    console.log('Started generating private key using RSA algorithm.');
    var keypairs = new rsa().generateKeyPair();
    var privatekey = keypairs.exportKey('private')
    // console.debug(privatekey)
    return privatekey
}

/******************************************************
 *  METHOD TO GENERATE JWT SIGN IN TOKEN
******************************************************/
function generateJWTtoken(privateKey)
{
    // dummy payload 
    var payload = {}
    payload.userName = 'samsudheen'
    payload.userId = '123123'
    payload.userRole = 'internaladmin'

    // dummy sign in options

    var signInOptions = {
        issuer: 'samsudheen',
        subject: 'sometest subjext',
        expiresIn: '24h',
        algorithm: "RS256"
    }

    try
    {
        var token = jwt.sign(payload, privateKey, signInOptions)
    }
    catch (err)
    {
        console.error('Failed to generate JWT token: '+ err)
    }
    
    // console.log(encrypt(token));
    return token
}