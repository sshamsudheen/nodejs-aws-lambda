# nodejs-aws-lambda

Create a lambda in AWS using node-js to read pemfile from aws s3 bucket , extract common name, public key and generate private key and create signin token using JWT and save it in AWS DynamoDB

## Pre-requesites

NodeJs > 12.

AWS account credentials.

AWS CLI, SAM CLI.


## NodeJs code

src/handler/index.js acts as the lambda handler and below node libraries are used in this project

```
const AWS               =   require('aws-sdk');
const bcrypt            =   require('bcryptjs');
const {createPublicKey} =   require('crypto');
const fs                =   require('fs');
const jwt               =   require('jsonwebtoken');
const nf                =   require('node-forge').pki;
const rsa               =   require('node-rsa');
```

## Steps to Clone, Configure and Deploy to AWS

**Clone**

```
git clone git@gitlab.com:sshamsudheen/nodejs-aws-lambda.git
cd nodejs-aws-lambda/
```

**Configure**

Provide cloudformation stack name in *samconfig.toml* , ***stack_name***, you CF stack will be created with this name.
Provide correct s3 bucket name in *samconfig.toml*, ***s3_bucket***, ***s3_prefix*** this bucket and key prefix is used by to save your template created by SAM package.
Provide correct region in *samconfig.toml*, ***region***, your stack and aws resources will be created in this region.

In *template.yaml*, provide the ***S3_bucket*** name under the ***Environment***, where the keys/certificate is saved and lambda will read the file from this s3 bucket.
Also in the Environment, provide the dynamo db table name ***Ddb_table_name***.

Once the configuration is done, use the below command to deploy the lambda, dynamodb table in AWS
Before running the below steps make sure your aws configuration is saved. check with command 'aws configure' command

**Deploy**
```
npm init
sam build 
sam deploy / sam deploy --guided
```

Once the above commands are executed successfully, check your AWS cloudformation stack and make sure the lambda and dynamodb table is created successfully.


To test your lambda invoke the lambda via console or using CLI.

While invoking the lambda you should invoke with the payload having ***certificate_name***. exaple below

```
{
  "certificate_name": "dormakabatt.crt"
}
```

## To Do / Improvements.

Unit test implementation is yet to be done.

Log only necessary details / information to cloudwatch.

Check and handle more exception if needed.